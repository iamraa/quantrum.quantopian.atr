"""
# Идея отсюда: http://www.investopedia.com/articles/trading/08/atr.asp

Доступны алгоритмы:
* test
* current_trade_signal
* filter_volume
* both_dir
* filter_sma200
* signal_at_next_day
* signal_by_close

Алгоритмы необходимо ставить первым параметром в эту функцию:
schedule_function({алгоритм}, date_rules.every_day(), time_rules.market_open(hours=1))

"""

import talib


# настраиваем алгоритм
def initialize(context):
    # 'XLY', 'XLK', 'XLI', 'XLB', 'XLE', 'XLP', 'XLV', 'XLU', 'XLF'
    context.stocks = symbols('TLT')  # 'DIA', 'SPY', 'QQQ', 'IWM'

    # 2x ATR(90) [-2] <> [-4] 'XLY', 'XLK', 'XLI', 'DIA', 'SPY', 'QQQ', 'IWM'

    context.open_atr = dict()
    context.opened_ago = dict()
    context.autoclose_in = 20

    # алгоритму разрешено только в лонг
    set_long_only()

    # торгуем спустя 1 час после начала торгов, пропустим высокую волатильность
    schedule_function(test, date_rules.every_day(), time_rules.market_open(hours=1))

    # готовимся собирать статистику
    init_test_stat(context)


# голый индикатор
def test(context, data):
    coef = 1. / len(context.stocks)

    # загружаем историю для рассчета ATR
    hist = data.history(context.stocks, ['open', 'high', 'low', 'close'], 500, '1d')

    for stock in context.stocks:
        # пропустим акцию, если нельзя торговать
        if not data.can_trade(stock):
            continue

        # проверим текущую позицию
        current_position = context.portfolio.positions[stock].amount

        # рассчитываем ATR за 14 дней
        atr_arr = talib.ATR(hist['high'][stock],
                        hist['low'][stock],
                        hist['close'][stock],
                        timeperiod=90)
        #atr = atr_arr[-1]
        #atr = atr_arr[-2] * 1.25
        atr = atr_arr[-2] * 2

        #price = data.current(stock, 'price')
        price = hist['close'][stock][-2]

        # используем цены предыдущего дня, так как уже есть цена за сегодня
        #prev_close = hist['close'][stock][-2]
        #prev_close = hist['close'][stock][-3]
        prev_close = hist['close'][stock][-4]

        # пробой вверх, покупка, когда цена выше предыдущей на 1х ATR
        upside_signal = price - (prev_close + atr)

        # пробой вниз, продажа, когда цена ниже предыдущей на 1х ATR
        downside_signal = prev_close - (price + atr)

        # проверяем рост ATR
        close_by_atr_growth = False
        if stock in context.open_atr:
            close_by_atr_growth = context.open_atr[stock]*2 < atr

        close_by_timeout = False
        if stock in context.opened_ago and context.opened_ago[stock]:
            context.opened_ago[stock] -= 1
            close_by_timeout = context.opened_ago[stock] <= 0
            #print(context.opened_ago[stock])

        # открываем позицию при пробое вверх
        if upside_signal > 0 and current_position <= 0:
            order_target_percent(stock, coef)
            #print(stock.symbol, 'buy')

            # сохраняем ATR для контроля роста
            context.open_atr[stock] = atr

            context.opened_ago[stock] = context.autoclose_in

        # закрываем позицию при пробое вниз
        # (downside_signal > 0 or close_by_atr_growth or close_by_timeout)
        elif close_by_timeout and current_position > 0:
            order_target_percent(stock, 0.0)
            #print(stock.symbol, 'sell')

            position = context.portfolio.positions[stock]
            context.stat(context, stock.symbol, key='res', val=round((position.last_sale_price / position.cost_basis - 1) * 100, 2))

        #context.opened_ago[stock] and current_position == 0
        #upside_signal > 0
        if upside_signal > 0:
            context.stat(context, stock.symbol, key='buy', val=(current_position == 0))
        if 0 and downside_signal > 0:
            context.stat(context, stock.symbol, key='sell')

        """
        record(
            'amount', current_position,
            'upside_signal', (100 if upside_signal > 0 else 0),
            'downside_signal', (-100 if downside_signal > 0 else 0),
        )
        """

    # выводим статистику в конце
    print_stat(context)



def current_trade_signal(context, data):
    """
    QQQ, IWM
    """
    coef = 1. / len(context.stocks)

    # загружаем историю для рассчета ATR
    hist = data.history(context.stocks, ['open', 'high', 'low', 'close'], 500, '1d')

    for stock in context.stocks:
        # пропустим акцию, если нельзя торговать
        if not data.can_trade(stock):
            continue

        # проверим текущую позицию
        current_position = context.portfolio.positions[stock].amount

        # рассчитываем ATR за 14 дней
        atr_arr = talib.ATR(hist['high'][stock],
                        hist['low'][stock],
                        hist['close'][stock],
                        timeperiod=14)
        atr = atr_arr[-1]
        #atr = atr_arr[-2] * 1.25

        price = data.current(stock, 'price')
        #price = hist['close'][stock][-2]

        # используем цены предыдущего дня, так как уже есть цена за сегодня
        prev_close = hist['close'][stock][-2]
        #prev_close = hist['close'][stock][-3]

        # пробой вверх, покупка, когда цена выше предыдущей на 1х ATR
        upside_signal = price - (prev_close + atr)

        # пробой вниз, продажа, когда цена ниже предыдущей на 1х ATR
        downside_signal = prev_close - (price + atr)

        # проверяем рост ATR
        close_by_atr_growth = False
        if stock in context.open_atr:
            close_by_atr_growth = context.open_atr[stock]*2 < atr

        close_by_timeout = False
        if stock in context.opened_ago and context.opened_ago[stock]:
            context.opened_ago[stock] -= 1
            close_by_timeout = context.opened_ago[stock] <= 0
            #print(context.opened_ago[stock])

        # открываем позицию при пробое вверх
        if upside_signal > 0 and current_position <= 0:
            order_target_percent(stock, coef)
            print(stock.symbol, 'buy')

            # сохраняем ATR для контроля роста
            context.open_atr[stock] = atr

            context.opened_ago[stock] = context.autoclose_in

        # закрываем позицию при пробое вниз
        elif (downside_signal > 0 or close_by_atr_growth or close_by_timeout) and current_position > 0:
            order_target_percent(stock, 0.0)
            print(stock.symbol, 'sell')


# фильтруем по объему
def filter_volume(context, data):
    coef = 1. / len(context.stocks)

    # загружаем историю для рассчета ATR
    hist = data.history(context.stocks, ['high', 'low', 'close', 'volume'], 500, '1d')

    for stock in context.stocks:
        # пропустим акцию, если нельзя торговать
        if not data.can_trade(stock):
            continue

        # проверим текущую позицию
        current_position = context.portfolio.positions[stock].amount

        # используем объем предыдущего дня
        #ma_volume = talib.SMA(hist['volume'][stock], timeperiod=50)[-2]
        volume = hist['volume'][stock][-2]
        prev_volume = hist['volume'][stock][-3]

        # рассчитываем ATR за 14 дней
        atr = talib.ATR(hist['high'][stock],
                        hist['low'][stock],
                        hist['close'][stock],
                        timeperiod=14)[-1]

        price = data.current(stock, 'price')

        # используем цены предыдущего дня, так как уже есть цена за сегодня
        prev_close = hist['close'][stock][-2]

        # пробой вверх, покупка, когда цена выше предыдущей на 1х ATR
        upside_signal = price - (prev_close + atr)

        # пробой вниз, продажа, когда цена ниже предыдущей на 1х ATR
        downside_signal = prev_close - (price + atr)

        # проверяем рост ATR
        close_by_atr_growth = False
        if stock in context.open_atr:
            close_by_atr_growth = context.open_atr[stock]*2 < atr

        # открываем позицию при пробое вверх
        if upside_signal > 0 and volume > prev_volume and current_position <= 0:
            order_target_percent(stock, coef)

            # сохраняем ATR для контроля роста
            context.open_atr[stock] = atr

        # закрываем позицию при пробое вниз
        elif (downside_signal > 0 or close_by_atr_growth) and current_position > 0:
            order_target_percent(stock, 0.0)

# в двух направлениях
def both_dir(context, data):
    coef = 1. / len(context.stocks)

    # загружаем историю для рассчета ATR
    hist = data.history(context.stocks, ['high', 'low', 'close'], 500, '1d')

    for stock in context.stocks:
        # пропустим акцию, если нельзя торговать
        if not data.can_trade(stock):
            continue

        # проверим текущую позицию
        current_position = context.portfolio.positions[stock].amount

        # рассчитываем ATR за 14 дней
        atr = talib.ATR(hist['high'][stock],
                        hist['low'][stock],
                        hist['close'][stock],
                        timeperiod=14)[-1]

        price = data.current(stock, 'price')

        # используем цены предыдущего дня, так как уже есть цена за сегодня
        prev_close = hist['close'][stock][-2]

        # пробой вверх, покупка, когда цена выше предыдущей на 1х ATR
        upside_signal = price - (prev_close + atr)

        # пробой вниз, продажа, когда цена ниже предыдущей на 1х ATR
        downside_signal = prev_close - (price + atr)

        # проверяем рост ATR
        close_by_atr_growth = False
        if stock in context.open_atr:
            close_by_atr_growth = context.open_atr[stock]*2 < atr

        close_by_timeout = False
        if stock in context.opened_ago and context.opened_ago[stock]:
            context.opened_ago[stock] -= 1
            close_by_timeout = context.opened_ago[stock] <= 0
            #print(context.opened_ago[stock])

        # открываем позицию при пробое вверх
        if upside_signal > 0 and current_position <= 0:
            order_target_percent(stock, coef)

            # сохраняем ATR для контроля роста
            context.open_atr[stock] = atr

            context.opened_ago[stock] = context.autoclose_in

        # закрываем позицию при пробое вниз
        elif downside_signal > 0 and current_position > 0:
            order_target_percent(stock, -coef)

        elif close_by_atr_growth or close_by_timeout:
            order_target_percent(stock, 0)


# ежедневная проверка
def filter_sma200(context, data):
    coef = 1. / len(context.stocks)

    # загружаем историю для рассчета ATR
    hist = data.history(context.stocks, ['high', 'low', 'close'], 500, '1d')

    for stock in context.stocks:
        # пропустим акцию, если нельзя торговать
        if not data.can_trade(stock):
            continue

        # проверим текущую позицию
        current_position = context.portfolio.positions[stock].amount

        ma_long = talib.SMA(hist['close'][stock], timeperiod=200)[-1]

        # рассчитываем ATR за 14 дней
        atr = talib.ATR(hist['high'][stock],
                        hist['low'][stock],
                        hist['close'][stock],
                        timeperiod=14)[-1]

        price = data.current(stock, 'price')

        # используем цены предыдущего дня, так как уже есть цена за сегодня
        prev_close = hist['close'][stock][-2]

        # пробой вверх, покупка, когда цена выше предыдущей на 1х ATR
        upside_signal = price - (prev_close + atr)

        # пробой вниз, продажа, когда цена ниже предыдущей на 1х ATR
        downside_signal = prev_close - (price + atr)

        # открываем позицию при пробое вверх
        if price > ma_long and upside_signal > 0 and current_position <= 0:
            order_target_percent(stock, coef)

        # закрываем позицию при пробое вниз
        elif downside_signal > 0 and current_position > 0:
            order_target_percent(stock, 0.0)


# открываем по сигналу на следующий день
def signal_at_next_day(context, data):
    coef = 1. / len(context.stocks)

    # загружаем историю для рассчета ATR
    hist = data.history(context.stocks, ['high', 'low', 'close'], 500, '1d')

    for stock in context.stocks:
        # пропустим акцию, если нельзя торговать
        if not data.can_trade(stock):
            continue

        # проверим текущую позицию
        current_position = context.portfolio.positions[stock].amount

        # рассчитываем ATR за 14 дней
        atr = talib.ATR(hist['high'][stock],
                        hist['low'][stock],
                        hist['close'][stock],
                        timeperiod=14)[-1]

        price = data.current(stock, 'price')

        # используем цены предыдущего дня, так как уже есть цена за сегодня
        prev_close = hist['close'][stock][-2]

        # пробой вверх, покупка, когда цена выше предыдущей на 1х ATR
        upside_signal = price - (prev_close + atr)

        # пробой вниз, продажа, когда цена ниже предыдущей на 1х ATR
        downside_signal = prev_close - (price + atr)

        # проверяем рост ATR
        close_by_atr_growth = False
        if stock in context.open_atr:
            close_by_atr_growth = context.open_atr[stock]*2 < atr

        close_by_timeout = False
        if stock in context.opened_ago and context.opened_ago[stock]:
            context.opened_ago[stock] -= 1
            close_by_timeout = context.opened_ago[stock] <= 0
            #print(context.opened_ago[stock])

        # открываем позицию при пробое вверх
        if upside_signal > 0 and current_position <= 0:
            # сохраняем ATR для контроля роста
            context.open_atr[stock] = atr

            context.opened_ago[stock] = context.autoclose_in

        # закрываем позицию при пробое вниз
        elif (downside_signal > 0 or close_by_atr_growth or close_by_timeout) and current_position > 0:
            order_target_percent(stock, 0.0)

        # open at next day after signal
        if stock in context.opened_ago and context.opened_ago[stock] > 0 and current_position <= 0:
            order_target_percent(stock, coef)


# получение сигнала по ценам закрытия
def signal_by_close(context, data):
    """
    Too many bad signals. Need to anlayze.

    """
    coef = 1. / len(context.stocks)

    # загружаем историю для рассчета ATR
    hist = data.history(context.stocks, ['high', 'low', 'close'], 500, '1d')

    for stock in context.stocks:
        # пропустим акцию, если нельзя торговать
        if not data.can_trade(stock):
            continue

        # проверим текущую позицию
        current_position = context.portfolio.positions[stock].amount

        # рассчитываем ATR за 14 дней
        atr_arr = talib.ATR(hist['high'][stock],
                        hist['low'][stock],
                        hist['close'][stock],
                        timeperiod=14)
        atr = atr_arr[-2]
        price = hist['close'][stock][-2]
        prev_close = hist['close'][stock][-3]

        # пробой вверх, покупка, когда цена выше предыдущей на 1х ATR
        upside_signal = price - (prev_close + atr)

        # пробой вниз, продажа, когда цена ниже предыдущей на 1х ATR
        downside_signal = prev_close - (price + atr_arr[-1])
        downside_signal = False

        # проверяем рост ATR
        close_by_atr_growth = False
        if stock in context.open_atr:
            close_by_atr_growth = context.open_atr[stock]*2 < atr

        close_by_timeout = False
        if stock in context.opened_ago and context.opened_ago[stock]:
            context.opened_ago[stock] -= 1
            close_by_timeout = context.opened_ago[stock] <= 0
            #print(context.opened_ago[stock])

        # открываем позицию при пробое вверх
        if upside_signal > 0 and current_position <= 0:
            order_target_percent(stock, coef)

            # сохраняем ATR для контроля роста
            context.open_atr[stock] = atr

            context.opened_ago[stock] = context.autoclose_in

        # закрываем позицию при пробое вниз
        elif (downside_signal > 0 or close_by_atr_growth or close_by_timeout) and current_position > 0:
            order_target_percent(stock, 0.0)
            #print(downside_signal > 0, close_by_atr_growth, close_by_timeout)

        record(
            'amount', current_position,
            'upside_signal', (100 if upside_signal > 0 else 0),
            'downside_signal', (-100 if downside_signal > 0 else 0),
        )

# подготовка для сбора данных о времени в позиции и кол-ве сделок
def init_test_stat(context):
    # берем дату окончания теста, чтобы вывести накопленные данные
    env = get_environment('*')
    context.end_date = env['end'].date()

    # готовим массив для логов
    context.stat = stat_accumulate


def stat_accumulate(context, symbol='na', key='na', val=0):
    if 'stat_arr' not in context:
        context.stat_arr = dict()
    if symbol not in context.stat_arr:
        context.stat_arr[symbol] = dict()
    if key not in context.stat_arr[symbol]:
        context.stat_arr[symbol][key] = dict()
    if 'cnt' not in context.stat_arr[symbol][key]:
        context.stat_arr[symbol][key]['cnt'] = 0
    if 'items' not in context.stat_arr[symbol][key]:
        context.stat_arr[symbol][key]['items'] = []
    context.stat_arr[symbol][key]['cnt'] += 1
    #return
    context.stat_arr[symbol][key]['items'].append(
        get_datetime().date().strftime('%y%m%d') + (" " + str(val) if val else ""))


# вывод статистики в последний день
def print_stat(context):
    # выводим лог в последний день
    if (get_datetime().date() == context.end_date):
        for k, v in context.stat_arr.iteritems():
            print(k, v)